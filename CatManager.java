package swHW3;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 * 
 */
public class CatManager
{
	int counter;
	
	private ArrayList<Cat> myCats = new ArrayList<Cat>();

	public CatManager()
	{
	
		Cat cat = new Cat("Fifi", "black");
		this.myCats.add(cat);
		System.out.println("Name: "+ myCats.get(0).getName()+ " Color: " +myCats.get(0).getColor());
		this.counter++;
		
		cat = new Cat("Fluffy", "spotted");
		this.myCats.add(cat);
		System.out.println("Name: "+ myCats.get(counter).getName()+ " Color: " +myCats.get(1).getColor());
		this.counter++;
		
		cat = new Cat("Josephine", "tabby");
		this.myCats.add(cat);
		System.out.println("Name: "+ myCats.get(2).getName()+ " Color: " +myCats.get(2).getColor());
		this.counter++;
		
		cat = new Cat("Biff", "tabby");
		this.myCats.add(cat);
		System.out.println("Name: "+ myCats.get(3).getName()+ " Color: " +myCats.get(3).getColor());
		this.counter++;
		
		cat = new Cat("Bumpkin", "white");
		this.myCats.add(cat);
		System.out.println("Name: "+ myCats.get(4).getName()+ " Color: " +myCats.get(4).getColor());
		this.counter++;
		
		Cat cat6 = new Cat("Spot", "spotted");
		this.myCats.add(cat6);
		System.out.println("Name: "+ myCats.get(5).getName()+ " Color: " +myCats.get(5).getColor());
		this.counter++;
		
		Cat cat7 = new Cat("Lulu", "tabby");
		this.myCats.add(cat7);this.counter++;
		
	}

	/** add a cat to the group
	 * 
	 * @param name
	 * @return
	 */
	public void add(Cat aCat)
	{
		counter++;
		this.myCats.add(counter - 1,aCat);

	}


	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
	 * 
	 * @param name - the name of the Cat that we are searching for
	 * @return the Cat object or null if not found
	 */
	public Cat findThisCat(String name)
	{
		for(Cat cats : myCats) {
			if (cats.getName().equals(name)) {
				return cats;
			}	
		}
		return null;	
		
	}

	// returns the number of cats of this color
	/**
	 * returns the number of cats of this color
	 * @param color - the color we are counting
	 * @return - the number of cats
	 */
	public int countColors(String color){ 
			int i = 0 ; 
			int counter2 = 0;
			if (myCats == null) {return 0;}
			while (counter2 < myCats.size()) {
				if (myCats.get(counter2).getColor() == color) {
					i++;}
			counter2++;	}
				
			return i;
				
	}
	
}
		
